var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'sala',
    database: 'todoapp'
});

var pool = mysql.createPool({
    connectionLimit : 100, // important
    host: 'localhost',
    user: 'root',
    password: 'sala',
    database: 'todoapp'
});

module.exports = pool;