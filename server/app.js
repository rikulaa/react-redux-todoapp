const express = require('express');
const path = require('path');
var bodyParser = require('body-parser')

const app = express();
// const connection = require('./db/connection.js');
const pool = require('./db/connection.js');

function handleDatabaseQuery(req, res) {
    pool.getConnection( (err, connection) => {
        console.log('connected as id ' + connection.threadId);
        // console.log(req);

        switch (req.method) {
            case 'GET':
                connection.query('SELECT * FROM todos ORDER BY created_at DESC', function (err, rows, fields) {
                    connection.release(); // We dont need it anymore so release to the pool
                    if (!err) {
                        res.json(rows);
                    };
                });
                break;
            case 'POST':

                if (req.path == '/api/todos/edit') {
                    console.log(req.path, req.body.data, req.body.id)
                    connection.query('UPDATE todos SET text=? WHERE id=?', [req.body.data, req.body.id], function (err, rows, fields) {
                    connection.release(); // We dont need it anymore so release to the pool
                    if (!err) {
                        res.json({"status": "ok"});
                        console.log(rows)
                    } else {
                        console.log(err)
                    }
                });
                } else if (req.path.includes('/api/todos/completed/')) {
                    connection.query('UPDATE todos SET completed = !completed WHERE id = ?', [req.params.id], function (err, rows, fields) {
                    connection.release(); // We dont need it anymore so release to the pool
                    if (!err) {
                        res.json({"status": "ok"});
                    }
                    });
                } else if (req.path == '/api/todos') {
                    console.log(req.body.data);
                    connection.query('INSERT INTO todos (text) VALUES (?)', [req.body.data], function (err, rows, fields) {
                    connection.release(); // We dont need it anymore so release to the pool
                    if (!err) {
                        res.json({"status": "ok"});
                    }
                    });
                }
                
                break;
            case 'DELETE':
                connection.query('DELETE FROM todos WHERE id = ?', [req.body.id], function(err, rows, fields) {
                    connection.release(); // We dont need it anymore so release to the pool
                    if (!err) {
                        res.json({"status": "ok"});
                    }
                });
                break;
        }
      

       connection.on('error', (err) => {
           res.json({"code": 100, status: "Error in connection database"});
           return;
       }) 
    });
};


const indexPage = path.resolve(__dirname, '..', 'public', 'index.html');

// parsing body of post request
// app.use(bodyParser.urlencoded({
    // extended: true
// }));
app.use(bodyParser.json());


// static assets
app.use(
    express.static(path.resolve(__dirname, '..', 'public'))
);

// Always return the main index.html, so react router can render the route in the client browser
app.get('/', (req, res) => {
    console.log('get request at *')
    res.sendFile(indexPage)
});

app.get('/todos', (req, res) => {
    res.sendFile(indexPage);
})

app.get('/api/todos', (req, res) => {
    console.log('request at api todos');
    // getAllPosts(req, res);
    handleDatabaseQuery(req, res);
});

app.post('/api/todos', (req, res) => {
    console.log('post at api/todos');
    // console.log(req.body.data);
    handleDatabaseQuery(req, res);
});

app.post('/api/todos/edit', (req, res) => {
    // console.log(req.path);
    // console.log(req.body.data);
    handleDatabaseQuery(req, res);
});

app.post('/api/todos/completed/:id', (req, res) => {
    console.log(req.params.id)
    handleDatabaseQuery(req, res);
})

app.delete('/api/todos', (req, res) => {
    console.log('delete at todos');
    console.log(req.body)
    handleDatabaseQuery(req, res);
});


module.exports = app;

