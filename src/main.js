import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, indexRoute, hashHistory, browserHistory } from 'react-router';

import { Provider } from 'react-redux';
import store from './store.js';

import App from './App.jsx';
import TodoList from './components/TodoList.jsx';



ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path='/' component={App}>
                <indexRoute component={App} />
            </Route>
                
            <Route path='todos' component={TodoList} />
            
        </Router>
    </Provider>,
    document.getElementById('main')
); 