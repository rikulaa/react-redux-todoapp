// import fetch from 'isomorphic-fetch';
import axios from 'axios';
/*

/*
Action creators
*/

export const REQUEST_TODOS = 'REQUEST_TODOS';
function fetchTodos() {
    return {
        type: REQUEST_TODOS
    }
}


export function requestTodos() {
    return function (dispatch) {
        dispatch(fetchTodos())
         return axios('/api/todos')
            // .then(response => response.json())
            // .then(json => dispatch(receiveTodos(json)))
            .then(response => dispatch(receiveTodos(response.data)))
    }

   
}

export const RECEIVE_TODOS = 'RECEIVE_TODOS';
function receiveTodos(json) {
    return {
        type: RECEIVE_TODOS,
        // posts: Object.keys(json.todos).map(function(todo) { return json.todos[todo] })
        // posts: json.map(todo => todo)
        posts: json
    }
}


export const ADD_TODO = 'ADD_TODO'
let nextTodoId = 3;
export function addTodo(text) {
    return function (dispatch) {
        // dispatch another function here
        return axios.post('/api/todos', {
            data: text
        })
        .catch(err => console.log(err))
        .then( response => dispatch(requestTodos()))

    }
    // return {
    //     type: ADD_TODO,
    //     text: text,
    //     id: nextTodoId++
    // }
};


export const REMOVE_TODO = 'REMOVE_TODO';
export function removeTodo(id) {
    return function(dispatch) {
        dispatch(removeTodoFromUi(id))
        return axios({
            method: 'delete',
            url: '/api/todos',
            data: { id: id }
        })
        .catch(err => console.log(err))
        // .then(dispatch(requestTodos()))
    }
};

function removeTodoFromUi(id) {
    return {
        type: REMOVE_TODO,
        id: id
    }
};

export function editTodo(id, text) {
    return function(dispatch) {
        // dispatch(removeTodoFromUi(id))
        return axios.post('api/todos/edit', {
            id: id,
            data: text
        })
        .catch(err => console.log(err))
        .then(response => dispatch(requestTodos()))
    }
}



export const TOGGLE_TODO = 'TOGGLE_TODO';
export function toggleTodo(id) {
    return function(dispatch) {
        dispatch(toggleTodoFromUi(id))
        return axios.post('/api/todos/completed/' + id)
        .catch(err => console.log(err))
        .then(response => dispatch(requestTodos()))
    }
};

function toggleTodoFromUi(id) {
    return {
        type: TOGGLE_TODO,
        id
    }
}