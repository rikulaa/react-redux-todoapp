import React from 'react';
import { Link } from 'react-router';
// require('bootstrap/dist/css/bootstrap.min.css');
import 'bootstrap/dist/css/bootstrap.min.css';
import './main.css';

var App = React.createClass({
    render: function() {
        return (
            <div id="landing-container" className="text-center">
                <h1>Hey welcome</h1>
                    <p>This is a simple todo -app. Click the link under to start saving your todos!</p>
                    <Link to='/todos'>Show todos!</Link>
            </div>
        );
    }
});


export default App;