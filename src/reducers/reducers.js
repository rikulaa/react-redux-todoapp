import { combineReducers } from 'redux';
import { REQUEST_TODOS, RECEIVE_TODOS, ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from '../actions/todolistActions';



const todo = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {
        id: action.id,
        text: action.text,
        completed: false
      }
    case 'TOGGLE_TODO':
      if (state.id !== action.id) {
        return state
      }

      return Object.assign({}, state, {
        completed: !state.completed
      })

      case 'REMOVE_TODO':
        if (state.id !== action.id) {
          return state
        } else return null

    default:
      return state
  }
}


function todos(state = [], action) {
  switch (action.type) {
    case REQUEST_TODOS:
      return [...state]
    case RECEIVE_TODOS:
        return action.posts
    // case ADD_TODO:
      // return [
        // todo(undefined, action),
        // ...state
      // ]
    case TOGGLE_TODO:
        return state.map(t =>
        todo(t, action)
      )
    case REMOVE_TODO:
      return state.filter((todoItem) =>
        todo(todoItem, action)
        // todoItem.id !== action.id
      )
    default:
      return state
  }
}

function app(state = {"loading": false }, action) {
    switch(action.type) {
      default:
        return state;
    }
}

const todoApp = combineReducers({
    todos,
    app
});

export default todoApp;