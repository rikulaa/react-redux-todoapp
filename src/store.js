import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'

import todoApp from './reducers/reducers.js';
// let store = createStore(todoApp);

// import { addTodo, removeTodo } from './actions/todolistActions.js';

const loggerMiddleware = createLogger()

const store = createStore(
    todoApp,
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        loggerMiddleware // neat middleware that logs actions
    )
)

// store.subscribe(function() {
    // console.log(store.getState())
// });

// store.dispatch(addTodo('Learn about actions'))
// store.dispatch(addTodo('Learn about reducers'))
// store.dispatch(addTodo('Learn about store'))

export default store;