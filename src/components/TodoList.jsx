import React from 'react';
import { Link } from 'react-router';

// connect with redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actionCreators from '../actions/todolistActions.js';
 
import { Button, Col, Form, FormControl, ListGroup, ListGroupItem, FormGroup, Pager, Label } from 'react-bootstrap';
import Todo from './Todo.jsx';

var TodoList = React.createClass({
    render: function() {
        
        // map todos and render them
        var todos = this.props.todos.map(function(todo) {
            var itemStyle = (todo.completed == true) ? "success" : "info"
            console.log(todo.completed)

            // Determine what todos to show (all, completed, Unfinished)
            switch (this.state.show) {
                case 'All':
                     return <Todo itemStyle={itemStyle} removeTodo={this.handleRemoveTodo.bind(this, todo.id)} editTodo={this.handleEditTodo.bind(this, todo.id)} ToggleComplete={this.handleToggleComplete.bind(this, todo.id)} key={todo.id} text={todo.text} />
                    //  break;
                case 'Unfinished':
                    return (todo.completed == false) ? <Todo itemStyle={itemStyle} removeTodo={this.handleRemoveTodo.bind(this, todo.id)} editTodo={this.handleEditTodo.bind(this, todo.id)} ToggleComplete={this.handleToggleComplete.bind(this, todo.id)} key={todo.id} text={todo.text} /> : ''
                    // break;
                case 'Completed':
                    return (todo.completed == true) ? <Todo itemStyle={itemStyle} removeTodo={this.handleRemoveTodo.bind(this, todo.id)} editTodo={this.handleEditTodo.bind(this, todo.id)} ToggleComplete={this.handleToggleComplete.bind(this, todo.id)} key={todo.id} text={todo.text} /> : ''
                    // break;
            }
            
           
        }.bind(this));

        return (
            <div className='col-md-6 col-md-offset-3'>
                <h2 className='col-md-6 col-md-offset-3'>Todos</h2>

                    <div className='row'>
                            <Form onSubmit={this.handleAddTodo}>
                                <Col md={10}>
                                <FormGroup>
                                    <FormControl
                                            type="text"
                                            placeholder="Write new add todo here"
                                            inputRef={ref => { this.todoInput = ref; }}
                                        />
                                </FormGroup>
                                    </Col>
                                <Col md={2}>
                                    <Button id="todo-submit-button" type='submit'>Add todo</Button>
                                </Col>
                            </Form>
                        </div>

                    <div className='row'>
                        <Col md={10}>
                            <ListGroup>
                                {todos}
                            </ListGroup>
                        </Col>
                    </div>

                    <div className='row'>
                        <Col md={10}>
                            <ListGroup>
                                <span id="view-buttons-header">View: </span>
                                <Button onClick={this.handleViewFilter.bind(null, 'All')} bsStyle={(this.state.show == 'All') ? "primary" : "link"}>All</Button>
                                <Button onClick={this.handleViewFilter.bind(null, 'Unfinished')}bsStyle={(this.state.show == 'Unfinished') ? "primary" : "link"}>Unfinished</Button>
                                <Button onClick={this.handleViewFilter.bind(null, 'Completed')} bsStyle={(this.state.show == 'Completed') ? "primary" : "link"}>Completed</Button>
                            </ListGroup>
                        </Col>
                    </div>

                    <div className='row'>
                        <Col md={10}>
                            <Button href='/'>Back</Button>
                            {/*<Button onClick={this.handleRequestTodos}>fetch all</Button>*/}
                        </Col>
                   </div>
                  
                   
                   
            </div>
        );
    },
    getInitialState: function() {
        return {
            show: 'All'
        }
    },
    componentDidMount: function() {
        this.props.actions.requestTodos();
    },

    handleViewFilter: function(filter) {
        this.setState({
            show: filter
        })
    },
    handleRequestTodos: function() {
        this.props.actions.requestTodos();
    },
 
    handleAddTodo: function(e) {
        e.preventDefault();
        var text = this.todoInput.value;
        this.todoInput.value = '';
        if (text.length > 0) {
            this.props.actions.addTodo(text);
        }
    },

    handleRemoveTodo: function(id) {
       this.props.actions.removeTodo(id)
    },
    handleEditTodo: function(id, data) {
        
        if (data !== '') {
            this.props.actions.editTodo(id, data)
            console.log(id, data);
        }
    },
    handleToggleComplete: function(id) {
        this.props.actions.toggleTodo(id)
    }
});


// connect todo -state from store, to component
function mapStateToProps(state) {
    return {
        todos: state.todos,
        // loading: state
    }
};

// connect todolistActions to component
function mapDispatchToProps(dispatch) {
  return  {
     actions: bindActionCreators(actionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);