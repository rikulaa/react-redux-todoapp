import React from 'react';
import { Button, Modal, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

var TodoModal = React.createClass({
    render: function() {
        return (
            <div>
                <Modal show={this.props.showModal}>
                    <Modal.Header>
                        <Modal.Title>Modal title</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form>
                            <FormGroup>
                            </FormGroup>
                        </form>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button bsStyle="primary">Save changes</Button>
                    </Modal.Footer>

                </Modal>
            </div>
        );
    }
});

export default TodoModal;