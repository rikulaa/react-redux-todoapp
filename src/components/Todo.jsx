import React, { Component, PropTypes } from 'react';
import { Col, ListGroupItem, Glyphicon, Button, Modal, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

class Todo extends Component {
    constructor(props) {
        super(props);

        this.state = {showModal: false}

        this.toggleModal = this.toggleModal.bind(this)
        // this.toggleComplete = this.toggleComplete.bind(this)
        this.prepareEditTodo = this.prepareEditTodo.bind(this)
    }

    toggleModal() {
          this.setState({
            showModal: (this.state.showModal) ? false : true
        });
    }

    // toggleComplete() {
        //  this.props.ToggleComplete
    // }

    prepareEditTodo() {
        console.log(this.textareaInput.value)
        var text = this.textareaInput.value;
        this.props.editTodo(text);
    }

    render() {
        return (
             <div>
                 <ListGroupItem bsStyle={this.props.itemStyle} onClick={this.toggleModal}>{this.props.text}
                     <span onClick={this.props.removeTodo} className="pull-right">
                         <Glyphicon glyph="remove"></Glyphicon>
                     </span>
                  
                 </ListGroupItem>
                <Modal show={this.state.showModal} onHide={this.toggleModal}>
                 
                     <Modal.Body>
                      
                         <form>
                             <FormGroup>
                                 <ControlLabel className="text-center">Edit your todo</ControlLabel>
                                 <FormControl inputRef={ref => { this.textareaInput = ref; }} defaultValue={this.props.text} componentClass="textarea">
                                 </FormControl>
                             </FormGroup>
                         </form>
                         <Button onClick={this.prepareEditTodo} bsStyle="default">Save</Button>
                     </Modal.Body>
                     <Modal.Footer>
                         <Button onClick={this.props.ToggleComplete} bsStyle="primary">Toggle completed</Button>
                         <Button onClick={this.props.removeTodo} bsStyle="default">Delete todo</Button>
                         <Button onClick={this.toggleModal} bsStyle="default">Close</Button>
                     </Modal.Footer>
                 </Modal>
             </div>
        );
    }
}

Todo.propTypes = {
    itemStyle: React.PropTypes.string,
    text: React.PropTypes.string,
    removeTodo: React.PropTypes.func,
    ToggleComplete: React.PropTypes.func

};

export default Todo;


// var Todo = React.createClass({
//     getInitialState: function() {
//         return {
//             showModal: false
//         }
//     },
//     toggleModal: function() {
//         this.setState({
//             showModal: (this.state.showModal) ? false : true
//         });
//     },
//     ToggleComplete: function() {
//         this.props.ToggleComplete
//     },
//     prepareEditTodo: function() {
//         console.log(this.textareaInput.value)
//         var text = this.textareaInput.value;
//         this.props.editTodo(text);
//     },
//     render: function() {
//         return (
//             <div>
//                 <ListGroupItem bsStyle={this.props.itemStyle} onClick={this.toggleModal}>{this.props.text}
//                     <span onClick={this.props.removeTodo} className="pull-right">
//                         <Glyphicon glyph="remove"></Glyphicon>
//                     </span>
                    
//                 </ListGroupItem>

//                <Modal show={this.state.showModal} onHide={this.toggleModal}>
                   

//                     <Modal.Body>
                        
//                         <form>
//                             <FormGroup>
//                                 <ControlLabel className="text-center">Edit your todo</ControlLabel>
//                                 <FormControl inputRef={ref => { this.textareaInput = ref; }} defaultValue={this.props.text} componentClass="textarea">
//                                 </FormControl>
//                             </FormGroup>
//                         </form>

//                         <Button onClick={this.prepareEditTodo} bsStyle="default">Save</Button>
//                     </Modal.Body>

//                     <Modal.Footer>
//                         <Button onClick={this.props.ToggleComplete} bsStyle="primary">Toggle completed</Button>
//                         <Button onClick={this.props.removeTodo} bsStyle="default">Delete todo</Button>
//                         <Button onClick={this.toggleModal} bsStyle="default">Close</Button>
//                     </Modal.Footer>

//                 </Modal>
//             </div>
//         );
//     }
// });



// export default Todo;