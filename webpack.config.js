var config = {
    entry: './src/main.js',

    output: {
        path: './public',
        filename: 'main.js'
    },

    devServer: {
        contentBase: './public',
        inline: true,
        port: 8080,
        host: "0.0.0.0",
        proxy: {
            '/': {
                target: 'http://localhost:9000',
                secure: false
            }
        }
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: '/node_module/',
                loader: 'babel',

                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css?$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /.woff$|.woff2$|.ttf$|.eot$|.svg$/,
                loader: 'url-loader'
            }
        ]
    }
}

module.exports = config;